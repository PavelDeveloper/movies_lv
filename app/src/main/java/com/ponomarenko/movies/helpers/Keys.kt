package com.ponomarenko.movies.helpers

enum class Keys(val value: String) {
    MovieId("movie_id"),
}