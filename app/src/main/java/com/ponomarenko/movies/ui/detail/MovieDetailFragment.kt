package com.ponomarenko.movies.ui.detail


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayerSupportFragmentX
import com.ponomarenko.movies.R
import com.ponomarenko.movies.common.Constants
import com.ponomarenko.movies.common.Constants.YOUTUBE_API_KEY
import com.ponomarenko.movies.helpers.Keys
import com.ponomarenko.movies.ui.detail.adapters.TrailerClickListener
import com.ponomarenko.movies.ui.detail.adapters.TrailersAdapter
import kotlinx.android.synthetic.main.movie_detail_fragment.*


class MovieDetailFragment : Fragment(), TrailerClickListener {

    companion object {
        fun newInstance() = MovieDetailFragment()
    }

    private lateinit var viewModel: MovieDetailViewModel
    private  val trailerAdapter = TrailersAdapter()
    private lateinit var youtubePlayer: com.google.android.youtube.player.YouTubePlayer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(MovieDetailViewModel::class.java)
        val view = inflater.inflate(R.layout.movie_detail_fragment, container, false)
        trailerAdapter.setClickListener(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureLayout()
        viewModel.fetchDetailMovie(movieId = arguments?.get(Keys.MovieId.value) as Int)
        viewModel.fetchTrailers(movieId = arguments?.get(Keys.MovieId.value) as Int)
        youtubeRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = trailerAdapter
        }
    }

    private fun configureLayout() {

        viewModel.detailMovie.observe(viewLifecycleOwner, Observer { movie ->
            movie?.let { movieDetail ->
            showImage(movieDetail.posterPath)
            nameTextView.text = movieDetail.originalTitle
            descriptionTv.text = movieDetail.overview
            rating.text = getString(R.string.rating)
                .replace("[RATING]", movieDetail.voteAverage.toString())
            releaseDate.text = getString(R.string.release).replace("[RELEASE]", movieDetail.releaseDate)
            productionCountries.text = getString(R.string.production).replace("[PRODUCTION]", movieDetail.productionCountries[0].country) // todo change to movieDetail.productionCountries
            duration.text = getString(R.string.duration).replace("[DURATION]", durationConverter(movieDetail.runtime))
            status.text = getString(R.string.status).replace("[STATUS]", movieDetail.status)
            genres.text = getString(R.string.genres).replace("[GENRES]", movieDetail.genres[0].name)
            spokenLanguages.text = getString(R.string.languages).replace("[LANGUAGES]",
                movieDetail.spokenLanguageRemotes[0].name)
            }
        })

        viewModel.trailerMovie.observe(viewLifecycleOwner, Observer {
            trailerAdapter.setData(it)
            if (it.isNotEmpty()) onItemClick(it[0].key)
        })

        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressBar.visibility = View.VISIBLE
            } else {
                progressBar.visibility = View.GONE
            }

        })
    }

     override fun onItemClick(key: String) {
         val youTubePlayerFragment: YouTubePlayerSupportFragmentX = YouTubePlayerSupportFragmentX.newInstance()
         youTubePlayerFragment.initialize(YOUTUBE_API_KEY, object : com.google.android.youtube.player.YouTubePlayer.OnInitializedListener {
             override fun onInitializationSuccess(
                 p0: com.google.android.youtube.player.YouTubePlayer.Provider?,
                 player: com.google.android.youtube.player.YouTubePlayer?,
                 wasRestored: Boolean
             ) {
                 youtubePlayer = player!!
                 youtubePlayer.fullscreenControlFlags = com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION
                 youtubePlayer.addFullscreenControlFlag(com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION)
                 youtubePlayer.addFullscreenControlFlag(com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI)
                 youtubePlayer.setFullscreen(false)
                 if (!wasRestored) {
                     youtubePlayer.loadVideo(key)
                 } else {
                     youtubePlayer.play()
                 }
             }

             override fun onInitializationFailure(p0: com.google.android.youtube.player.YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
             }
         })
         childFragmentManager.beginTransaction()
             .replace(R.id.youtube_player_fragment, youTubePlayerFragment).commit()
    }

    private fun showImage(imgPath: String) {
        Glide.with(requireActivity())
            .load(Constants.POSTER_BASE_URL + imgPath)
            .error(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.ic_launcher_background
                )
            )
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(movieImage)
    }

    private fun durationConverter(t: Int): String {
        val hours = t / 60
        val minutes = t % 60
        return "${hours}h${minutes}m"
    }
}