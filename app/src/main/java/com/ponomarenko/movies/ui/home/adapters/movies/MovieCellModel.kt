package com.ponomarenko.movies.ui.home.adapters.movies

import com.ponomarenko.movies.domain.models.MovieModel

data class MovieCellModel(
    val id: Int,
    val genreIds: List<Int>,
    val originalTitle: String = "",
    val popularity: Double,
    val posterPath: String = "",
    val originalLanguage: String = "",
    val title: String = ""
)

fun MovieModel.mapToUI(): MovieCellModel {
    return MovieCellModel(
        id = this.id,
        genreIds = this.genreIds,
        originalTitle = this.originalTitle,
        originalLanguage = this.originalLanguage,
        popularity = this.popularity,
        posterPath = this.posterPath,
        title = this.title
    )
}