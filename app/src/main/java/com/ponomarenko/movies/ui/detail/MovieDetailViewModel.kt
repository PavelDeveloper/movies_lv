package com.ponomarenko.movies.ui.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ponomarenko.movies.domain.db.AppDatabase
import com.ponomarenko.movies.domain.db.DetailMovieDao
import com.ponomarenko.movies.domain.models.DetailMovieModel
import com.ponomarenko.movies.domain.models.TrailerModel
import com.ponomarenko.movies.domain.repositories.details.DetailMovieRepositoryImpl
import com.ponomarenko.movies.domain.repositories.trailers.TrailersRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieDetailViewModel(application: Application) : AndroidViewModel(application) {

    private val detailMovieDao: DetailMovieDao
    private val detailMovieRepository: DetailMovieRepositoryImpl
    private val trailersRepositoryImpl: TrailersRepositoryImpl

    private val _detailMovie = MutableLiveData<DetailMovieModel>()
    private val _trailerMovie = MutableLiveData<List<TrailerModel>>()
    private val _isLoading = MutableLiveData<Boolean>().apply { value = false }

    val detailMovie: LiveData<DetailMovieModel> = _detailMovie
    val trailerMovie: LiveData<List<TrailerModel>> = _trailerMovie
    val isLoading: LiveData<Boolean> = _isLoading

    init {
        detailMovieDao = AppDatabase.getDatabase(application).detailMovieDao()
        detailMovieRepository = DetailMovieRepositoryImpl(detailMovieDao = detailMovieDao)
        trailersRepositoryImpl = TrailersRepositoryImpl()
    }

    fun fetchDetailMovie(movieId: Int) {
        viewModelScope.launch {
            _isLoading.postValue(true)
            withContext(Dispatchers.Default) {
                val detailData = detailMovieRepository.fetchDetailData(movieId)
                _detailMovie.postValue(detailData)
                _isLoading.postValue(false)
            }
        }
    }

    fun fetchTrailers(movieId: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val trailers = trailersRepositoryImpl.fetchTrailers(movieId)
                _trailerMovie.postValue(trailers)
            }
        }
    }

}