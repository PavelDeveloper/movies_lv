package com.ponomarenko.movies.ui.home.adapters.genres

import com.ponomarenko.movies.domain.models.GenreModel

data class GenreCellModel(val id: Int, val name: String)

fun GenreModel.mapToUI(): GenreCellModel {
    return GenreCellModel(
        id = this.id,
        name = this.name
    )
}