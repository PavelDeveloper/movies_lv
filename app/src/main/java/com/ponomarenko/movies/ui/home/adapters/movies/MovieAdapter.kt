package com.ponomarenko.movies.ui.home.adapters.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ponomarenko.movies.R
import com.ponomarenko.movies.common.Constants.POSTER_BASE_URL
import com.ponomarenko.movies.helpers.Keys
import kotlinx.android.synthetic.main.cell_movie.view.*

class MovieAdapter(val genreMovieDataList: ArrayList<MovieCellModel>) :
    RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private val movieDataList: ArrayList<MovieCellModel> = ArrayList()
    private var moviesGenreList = ArrayList<MovieCellModel>()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(inflater.inflate(R.layout.cell_movie, parent, false))
    }

    override fun getItemCount() = genreMovieDataList.count()

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(model = genreMovieDataList[position])
        holder.imgMoviePoster.setOnClickListener {
            showDetail(it, genreMovieDataList[position].id)
        }
    }

    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imgMoviePoster: ImageView = itemView.imgMoviePoster
        private val txtMovieName: TextView = itemView.txtMovieName
        private val txtMoviePopularity: TextView = itemView.txtMoviePopularity
        private val txtMovLang: TextView = itemView.txtMovLang

        fun bind(model: MovieCellModel) {

            txtMovieName.text = model.originalTitle
            txtMoviePopularity.text = itemView.context.getString(R.string.rating)
                .replace("[RATING]", model.popularity.toString())
            txtMovLang.text = itemView.context.getString(R.string.language)
                .replace("[LANGUAGE]", model.originalLanguage)

            Glide.with(itemView.context)
                .load(POSTER_BASE_URL + model.posterPath)
                .error(
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.ic_launcher_background
                    )
                )
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(imgMoviePoster)
        }
    }

    private fun showDetail(sender: View, movieId: Int) {
        sender.findNavController().navigate(R.id.action_homeFragment_to_movieDetailFragment,
            Bundle().apply {
                this.putInt(Keys.MovieId.value, movieId)
            }
        )
    }
}