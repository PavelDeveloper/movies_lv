package com.ponomarenko.movies.ui.home

import android.app.Application
import androidx.lifecycle.*
import com.ponomarenko.movies.domain.db.AppDatabase
import com.ponomarenko.movies.domain.db.GenreDao
import com.ponomarenko.movies.domain.db.MovieDao
import com.ponomarenko.movies.domain.repositories.genres.GenreRepositoryImpl
import com.ponomarenko.movies.domain.repositories.movies.MovieRepositoryImpl
import com.ponomarenko.movies.ui.home.adapters.genres.GenreCellModel
import com.ponomarenko.movies.ui.home.adapters.genres.mapToUI
import com.ponomarenko.movies.ui.home.adapters.movies.MovieCellModel
import com.ponomarenko.movies.ui.home.adapters.movies.mapToUI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json.Default.context
import okhttp3.Dispatcher

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private var genreRepository: GenreRepositoryImpl
    private var movieRepository: MovieRepositoryImpl
    private var moviesDao: MovieDao
    private var genresDao: GenreDao

    private val _genres = MutableLiveData<List<GenreCellModel>>().apply { value = ArrayList() }
    private val _movies = MutableLiveData<List<MovieCellModel>>().apply { value = ArrayList() }
    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }

    val genres: LiveData<List<GenreCellModel>> = _genres
    val movies: LiveData<List<MovieCellModel>> = _movies
    val isLoading: LiveData<Boolean> = _isLoading

    init {
        genresDao = AppDatabase.getDatabase(application).genreDao()
        genreRepository = GenreRepositoryImpl(genreDao = genresDao)

        moviesDao = AppDatabase.getDatabase(application).movieDao()
        movieRepository = MovieRepositoryImpl(movieDao = moviesDao)
    }

    fun fetchGenres() {
        viewModelScope.launch {
            _isLoading.postValue(true)
            withContext(Dispatchers.Default) {
                val genresRemote = genreRepository.fetchGenres()
                _isLoading.postValue(false)
                _genres.postValue(genresRemote.map { it.mapToUI() })
            }
        }
    }

    fun fetchMovies() {
        viewModelScope.launch {
            _isLoading.postValue(true)
            withContext(Dispatchers.Default) {
                val moviesRemote = movieRepository.fetchMovies()
                _isLoading.postValue(false)
                _movies.postValue(moviesRemote.map { it.mapToUI() })
            }
        }
    }

}