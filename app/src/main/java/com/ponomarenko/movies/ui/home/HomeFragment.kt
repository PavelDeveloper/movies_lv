package com.ponomarenko.movies.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ponomarenko.movies.R
import com.ponomarenko.movies.ui.home.adapters.genres.GenreAdapter
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment() : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private val genreAdapter = GenreAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchGenres()
        viewModel.fetchMovies()

        setupData()
        setupLoading()

        context?.let {
            recyclerGenres.adapter = genreAdapter
            recyclerGenres.layoutManager =
                LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
        }

    }

    private fun setupLoading() {
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressGenre.visibility = View.VISIBLE
            } else {
                progressGenre.visibility = View.GONE
            }
        })
    }

    private fun setupData() {
        viewModel.genres.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                genreAdapter.setData(newData = it)
            }
        })
        viewModel.movies.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) genreAdapter.setMovieData(movieData = it)
        })
    }
}