package com.ponomarenko.movies.ui.detail.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ponomarenko.movies.R
import com.ponomarenko.movies.common.Constants
import com.ponomarenko.movies.domain.models.TrailerModel
import kotlinx.android.synthetic.main.cell_trailer.view.*

class TrailersAdapter: RecyclerView.Adapter<TrailersAdapter.TrailerViewHolder>() {

    private val trailerList = ArrayList<TrailerModel>()

    private var listener: TrailerClickListener? = null

    fun setClickListener(listener: TrailerClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TrailersAdapter.TrailerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TrailerViewHolder(inflater.inflate(R.layout.cell_trailer, parent, false))
    }

    override fun getItemCount() = trailerList.count()

    override fun onBindViewHolder(holder: TrailersAdapter.TrailerViewHolder, position: Int) {
        holder.bind(model = trailerList[position])
        holder.itemView.trailerImageView.setOnClickListener {
            listener?.onItemClick(trailerList[position].key)
        }
    }

    fun setData(trailers: List<TrailerModel>) {
        trailerList.clear()
        trailerList.addAll(trailers)
        notifyDataSetChanged()
    }

    inner class TrailerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val trailerImg: ImageView = itemView.trailerImageView
        private val trailerTxt: TextView = itemView.trailerName

        fun bind(model: TrailerModel) {

            Glide.with(itemView.context)
                .load(java.lang.String.format(Constants.YOUTUBE_IMAGE_URL, model.key))
                .error(
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.ic_launcher_background
                    )
                )
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(trailerImg)
            trailerTxt.text = model.name
        }
    }
}

interface TrailerClickListener {
    fun onItemClick(key: String)
}