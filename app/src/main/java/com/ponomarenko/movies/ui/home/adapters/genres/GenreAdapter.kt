package com.ponomarenko.movies.ui.home.adapters.genres

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ponomarenko.movies.R
import com.ponomarenko.movies.ui.home.adapters.movies.MovieAdapter
import com.ponomarenko.movies.ui.home.adapters.movies.MovieCellModel
import kotlinx.android.synthetic.main.cell_genre.view.*

class GenreAdapter : RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {

    private val genreDataList = ArrayList<GenreCellModel>()
    private val genreMovieDataList = ArrayList<MovieCellModel>()
    private val viewPool = RecyclerView.RecycledViewPool()

    fun setData(newData: List<GenreCellModel>) {
        genreDataList.clear()
        genreDataList.addAll(newData)
        notifyDataSetChanged()
    }

    fun setMovieData(movieData: List<MovieCellModel>) {
        genreMovieDataList.clear()
        genreMovieDataList.addAll(movieData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GenreViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return GenreViewHolder((inflater.inflate(R.layout.cell_genre, parent, false)))
    }

    override fun getItemCount() = genreDataList.count()

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        holder.bind(model = genreDataList[position])
        holder.recyclerView.apply {
            layoutManager = LinearLayoutManager(
                holder.recyclerView.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            val movies = genreMovieDataList.filter { movie ->
                movie.genreIds.contains(genreDataList[position].id)
            }
            adapter = MovieAdapter(genreMovieDataList = movies as ArrayList<MovieCellModel>)
            setRecycledViewPool(viewPool)
        }
    }

    inner class GenreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtName: TextView = itemView.txtGenreName
        val recyclerView: RecyclerView = itemView.recyclerMovies

        fun bind(model: GenreCellModel) {
            txtName.text = model.name
        }
    }
}