package com.ponomarenko.movies.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ponomarenko.movies.data.models.GenreRemote

@Entity(tableName = "genres")
data class GenreModel(
    @field:PrimaryKey
    val id: Int,
    val name: String = ""
)

fun GenreRemote.mapToDomain(): GenreModel {
    return GenreModel(
        id = this.id,
        name = this.name
    )
}