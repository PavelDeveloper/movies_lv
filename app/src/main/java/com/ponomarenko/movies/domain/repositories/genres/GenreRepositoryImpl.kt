package com.ponomarenko.movies.domain.repositories.genres

import com.ponomarenko.movies.data.network.RetrofitFactory
import com.ponomarenko.movies.domain.db.GenreDao
import com.ponomarenko.movies.domain.models.GenreModel
import com.ponomarenko.movies.domain.models.mapToDomain

class GenreRepositoryImpl(private val genreDao: GenreDao) : GenreRepository {

    private var genresRemote: List<GenreModel> = ArrayList()

    override suspend fun fetchGenres(): List<GenreModel> {
        try {
             genresRemote =
                RetrofitFactory.instance.genresService.getGenres().genres.map { it.mapToDomain() }
            if (genresRemote.isNotEmpty()) {
                for (genre in genresRemote) {
                    genreDao.insertAll(genre)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return when {
            genresRemote.isNotEmpty() -> {
                genresRemote
            }
            else -> genreDao.all()
        }
    }
}