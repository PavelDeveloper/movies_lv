package com.ponomarenko.movies.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ponomarenko.movies.data.models.MovieRemote

@Entity(tableName = "movies")
data class MovieModel(
    @field:PrimaryKey
    val id: Int,
    val genreIds: List<Int>,
    val originalLanguage: String = "",
    val originalTitle: String = "",
    val overview: String = "",
    val popularity: Double,
    val posterPath: String = "",
    val releaseDate: String = "",
    val title: String = ""
)

fun MovieRemote.mapToDomain(): MovieModel {
    return MovieModel(
        id = this.id,
        genreIds = this.genreIds,
        originalLanguage = this.originalLanguage,
        originalTitle = this.originalTitle,
        overview = this.overview,
        popularity = this.popularity,
        posterPath = this.posterPath,
        releaseDate = this.releaseDate,
        title = this.title
    )
}
