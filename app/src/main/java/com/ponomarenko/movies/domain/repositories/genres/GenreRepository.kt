package com.ponomarenko.movies.domain.repositories.genres

import com.ponomarenko.movies.domain.models.GenreModel

interface GenreRepository {
    suspend fun fetchGenres(): List<GenreModel>?
}