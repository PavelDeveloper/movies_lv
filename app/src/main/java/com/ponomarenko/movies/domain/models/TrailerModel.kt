package com.ponomarenko.movies.domain.models

import com.ponomarenko.movies.data.models.TrailerRemote

data class TrailerModel(
    val id: String = "1",
    val key: String = "",
    val name: String
)

fun TrailerRemote.mapToDomain(): TrailerModel = TrailerModel(
    id = this.id,
    key = this.key,
    name = this.name
)