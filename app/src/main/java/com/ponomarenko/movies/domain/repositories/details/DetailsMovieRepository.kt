package com.ponomarenko.movies.domain.repositories.details

import com.ponomarenko.movies.domain.models.DetailMovieModel

interface DetailsMovieRepository {
    suspend fun fetchDetailData(movieId: Int): DetailMovieModel?
}