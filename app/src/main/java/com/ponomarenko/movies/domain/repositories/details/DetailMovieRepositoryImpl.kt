package com.ponomarenko.movies.domain.repositories.details

import com.ponomarenko.movies.data.network.RetrofitFactory
import com.ponomarenko.movies.domain.db.DetailMovieDao
import com.ponomarenko.movies.domain.models.DetailMovieModel
import com.ponomarenko.movies.domain.models.mapToDomain
import java.lang.Exception

class DetailMovieRepositoryImpl(private val detailMovieDao: DetailMovieDao): DetailsMovieRepository {

    private var detailMovieRemote: DetailMovieModel? = null

    override suspend fun fetchDetailData(movieId: Int): DetailMovieModel? {

        try {
             detailMovieRemote = RetrofitFactory.instance.detailMovieService
                .getDetailMovieData(movieId = movieId).mapToDomain()
            detailMovieRemote?.let {
                detailMovieDao.insert(it)
            }
        } catch (e:Exception) {e.printStackTrace()}
        return when{
            detailMovieRemote != null -> detailMovieRemote
            else -> detailMovieDao.getDetailMovieById(movieId)
        }
    }

}