package com.ponomarenko.movies.domain.db

import androidx.room.*
import com.ponomarenko.movies.domain.models.GenreModel

@Dao
interface GenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg model: GenreModel)

    @Query("SELECT * FROM genres")
    suspend fun all(): List<GenreModel>

    @Query("SELECT * FROM genres WHERE id = :id")
    suspend fun getGenreById(id: Int): GenreModel

    @Query("DELETE FROM genres WHERE id = :id")
    suspend  fun deleteById(id: Int)

    @Delete
    suspend fun reallyDeleteGenreData(data: GenreModel)
}