package com.ponomarenko.movies.domain.repositories.movies

import com.ponomarenko.movies.data.network.RetrofitFactory
import com.ponomarenko.movies.domain.db.MovieDao
import com.ponomarenko.movies.domain.models.MovieModel
import com.ponomarenko.movies.domain.models.mapToDomain

class MovieRepositoryImpl(private val movieDao: MovieDao) : MovieRepository {

    private var movies: ArrayList<MovieModel> = ArrayList()

    override suspend fun fetchMovies(): List<MovieModel> {
        movies.clear()
        try {
            val moviesRemote = RetrofitFactory.instance.popularMoviesService
                .getPopularMovies().results.map { it.mapToDomain() }
            movies.addAll(moviesRemote)
            if (moviesRemote.isNotEmpty()) {
                for (movie in moviesRemote) {
                    movieDao.insertAll(movie)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return when {
            movies.isNotEmpty() -> {
                movies
            }
            else -> movieDao.all()
        }
    }
}