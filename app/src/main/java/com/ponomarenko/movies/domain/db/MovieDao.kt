package com.ponomarenko.movies.domain.db

import androidx.room.*
import com.ponomarenko.movies.domain.models.MovieModel

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg model: MovieModel)

    @Query("SELECT * FROM movies")
    suspend fun all(): List<MovieModel>

    @Query("SELECT * FROM movies WHERE id = :id")
    suspend fun getMovieById(id: Int): MovieModel

    @Query("DELETE FROM movies WHERE id = :id")
    suspend fun deleteById(id: Int)

    @Delete
    suspend fun reallyDeleteMovieData(data: MovieModel)
}