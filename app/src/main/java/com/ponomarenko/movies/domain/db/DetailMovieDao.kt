package com.ponomarenko.movies.domain.db

import androidx.room.*
import com.ponomarenko.movies.domain.models.DetailMovieModel

@Dao
interface DetailMovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg model: DetailMovieModel)

    @Query("SELECT * FROM detail")
    suspend fun all(): List<DetailMovieModel>

    @Query("SELECT * FROM detail WHERE id = :id")
    suspend fun getDetailMovieById(id: Int): DetailMovieModel

    @Query("DELETE FROM detail WHERE id = :id")
    suspend fun deleteById(id: Int)

    @Delete
    suspend fun reallyDeleteDetailMovieData(data: DetailMovieModel)
}
