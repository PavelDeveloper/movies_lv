package com.ponomarenko.movies.domain.repositories.movies

import com.ponomarenko.movies.domain.models.MovieModel

interface MovieRepository {
    suspend fun fetchMovies(): List<MovieModel>?
}