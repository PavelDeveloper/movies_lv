package com.ponomarenko.movies.domain.repositories.trailers

import com.ponomarenko.movies.data.network.RetrofitFactory
import com.ponomarenko.movies.domain.models.TrailerModel
import com.ponomarenko.movies.domain.models.mapToDomain

class TrailersRepositoryImpl : TrailersRepository {

    private var trailers: List<TrailerModel> = ArrayList()

    override suspend fun fetchTrailers(movieId: Int): List<TrailerModel> {
        try {
             trailers = RetrofitFactory.instance.movieTrailersService
                .getMovieTrailers(movieId = movieId)
                .results.map { it.mapToDomain() }
        } catch (e: Exception) {e.printStackTrace()}

        return trailers
    }
}