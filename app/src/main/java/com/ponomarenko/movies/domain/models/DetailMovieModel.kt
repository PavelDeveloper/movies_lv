package com.ponomarenko.movies.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ponomarenko.movies.data.models.*

@Entity(tableName = "detail")
data class DetailMovieModel(
    @field:PrimaryKey
    val id: Int,
    val budget: Int,
    val genres: List<GenreRemote>,
    val originalTitle: String = "",
    val popularity: Double,
    val posterPath: String = "",
    val productionCompanyRemotes: List<ProductionCompanyRemote>,
    val productionCountries: List<ProductionCountryRemote>,
    val releaseDate: String = "",
    val spokenLanguageRemotes: List<SpokenLanguageRemote>,
    val status: String = "",
    val tagline: String = "",
    val title: String = "",
    val overview: String = "",
    val runtime: Int,
    val voteAverage: Double
)

fun MovieDetailRemote.mapToDomain(): DetailMovieModel {
    return DetailMovieModel(
        id = this.id,
        budget = this.budget,
        genres = this.genres,
        originalTitle = this.originalTitle,
        popularity = this.popularity,
        posterPath = this.posterPath,
        productionCompanyRemotes = this.productionCompanyRemotes,
        productionCountries = this.productionCountries,
        releaseDate = this.releaseDate,
        spokenLanguageRemotes = this.spokenLanguageRemotes,
        status = this.status,
        tagline = this.tagline,
        title = this.title,
        overview = this.overview,
        runtime = this.runtime,
        voteAverage = this.voteAverage
    )
}
