package com.ponomarenko.movies.domain.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ponomarenko.movies.data.models.GenreRemote
import com.ponomarenko.movies.data.models.ProductionCompanyRemote
import com.ponomarenko.movies.data.models.ProductionCountryRemote
import com.ponomarenko.movies.data.models.SpokenLanguageRemote

class Converters {
    companion object {
        @TypeConverter
        @JvmStatic
        fun fromOptionGenreIdsList(genreValues: List<Int>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<Int>>() {}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionGenreIdsList(optionValuesString: String): List<Int>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<Int>>() {}.type
            return gson.fromJson(optionValuesString, listType)
        }


        @TypeConverter
        @JvmStatic
        fun fromOptionGenreRemoteList(genreValues: List<GenreRemote>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<GenreRemote>>() {}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionGenreRemoteList(optionValuesString: String): List<GenreRemote>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<GenreRemote>>() {}.type
            return gson.fromJson(optionValuesString, listType)
        }


    @TypeConverter
        @JvmStatic
        fun fromOptionProductionCompanyRemoteList(genreValues: List<ProductionCompanyRemote>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCompanyRemote>>() {}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionProductionCompanyRemoteList(optionValuesString: String): List<ProductionCompanyRemote>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCompanyRemote>>() {}.type
            return gson.fromJson(optionValuesString, listType)
        }


    @TypeConverter
        @JvmStatic
        fun fromOptionProductionCountryRemoteList(genreValues: List<ProductionCountryRemote>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCountryRemote>>() {}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionProductionCountryRemoteList(optionValuesString: String): List<ProductionCountryRemote>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<ProductionCountryRemote>>() {}.type
            return gson.fromJson(optionValuesString, listType)
        }

    @TypeConverter
        @JvmStatic
        fun fromOptionSpokenLanguageRemoteList(genreValues: List<SpokenLanguageRemote>?): String? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<SpokenLanguageRemote>>() {}.type
            return gson.toJson(genreValues, listType)
        }

        @TypeConverter
        @JvmStatic
        fun toOptionSpokenLanguageRemoteList(optionValuesString: String): List<SpokenLanguageRemote>? {
            val gson = Gson()
            val listType = object : TypeToken<ArrayList<SpokenLanguageRemote>>() {}.type
            return gson.fromJson(optionValuesString, listType)
        }
    }
}