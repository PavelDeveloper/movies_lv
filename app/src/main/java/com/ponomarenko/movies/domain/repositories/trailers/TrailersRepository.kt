package com.ponomarenko.movies.domain.repositories.trailers

import com.ponomarenko.movies.domain.models.TrailerModel

interface TrailersRepository {
    suspend fun fetchTrailers(movieId: Int): List<TrailerModel>
}