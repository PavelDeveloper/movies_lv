package com.ponomarenko.movies.data.models

import kotlinx.serialization.Serializable

@Serializable
data class TrailerRemote(
    val id: String = "",
    val iso_3166_1: String = "",
    val iso_639_1: String = "",
    val key: String = "",
    val name: String = "",
    val site: String = "",
    val size: Int,
    val type: String = ""
)