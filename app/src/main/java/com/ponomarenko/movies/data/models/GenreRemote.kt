package com.ponomarenko.movies.data.models

import kotlinx.serialization.Serializable

@Serializable
data class GenreRemote(
    val id: Int,
    val name: String = ""
)