package com.ponomarenko.movies.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ProductionCompanyRemote(
    val id: Int,
    @SerialName("logo_path") val logoPath: String?,
    val name: String = "",
    val origin_country: String = ""
)