package com.ponomarenko.movies.data.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.ponomarenko.movies.common.Constants.BASE_URL
import com.ponomarenko.movies.data.services.DetailMovieService
import com.ponomarenko.movies.data.services.GenresService
import com.ponomarenko.movies.data.services.MovieTrailersService
import com.ponomarenko.movies.data.services.PopularMoviesService
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class RetrofitFactory {

    companion object {
        const val key = "75cb25a125d25ee5271f3f5bdbb07415"
        val instance = RetrofitFactory()
    }

    private fun okHttpInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(okHttpInterceptor())
        .connectTimeout(30, TimeUnit.SECONDS)
        .retryOnConnectionFailure(true)
        .build()

    private val retrofitClient:  Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
        .build()

    val popularMoviesService: PopularMoviesService = retrofitClient.create(PopularMoviesService::class.java)
    val genresService: GenresService = retrofitClient.create(GenresService::class.java)
    val detailMovieService: DetailMovieService = retrofitClient.create(DetailMovieService::class.java)
    val movieTrailersService: MovieTrailersService = retrofitClient.create(MovieTrailersService::class.java)
}