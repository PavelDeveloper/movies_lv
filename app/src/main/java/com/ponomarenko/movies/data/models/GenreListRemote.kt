package com.ponomarenko.movies.data.models

import kotlinx.serialization.Serializable

@Serializable
data class GenreListRemote(
    val genres: List<GenreRemote>
)