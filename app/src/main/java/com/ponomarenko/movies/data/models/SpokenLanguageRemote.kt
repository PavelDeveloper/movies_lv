package com.ponomarenko.movies.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SpokenLanguageRemote(
    @SerialName("iso_639_1") val iso: String = "",
    val name: String = ""
)