package com.ponomarenko.movies.data.models

import kotlinx.serialization.Serializable

@Serializable
data class MovieTrailerListRemote(
    val id: Int,
    val results: List<TrailerRemote>
)