package com.ponomarenko.movies.data.models

import kotlinx.serialization.Serializable

@Serializable
data class MovieListRemote(
    val page: Int,
    val results: List<MovieRemote>,
    val total_pages: Int,
    val total_results: Int
)