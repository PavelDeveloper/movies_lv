package com.ponomarenko.movies.data.services

import com.ponomarenko.movies.common.Constants.API_KEY
import com.ponomarenko.movies.data.models.MovieDetailRemote
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DetailMovieService {
    @GET("movie/{id}")
    suspend fun getDetailMovieData(@Path("id") movieId: Int,
                           @Query("api_key") api_key: String = API_KEY): MovieDetailRemote

}