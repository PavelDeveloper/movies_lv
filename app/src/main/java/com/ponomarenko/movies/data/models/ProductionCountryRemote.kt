package com.ponomarenko.movies.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ProductionCountryRemote(
    @SerialName("iso_3166_1") val country: String,
    val name: String?
)