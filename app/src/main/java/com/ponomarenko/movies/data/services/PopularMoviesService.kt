package com.ponomarenko.movies.data.services

import com.ponomarenko.movies.common.Constants
import com.ponomarenko.movies.data.models.MovieListRemote
import retrofit2.http.GET
import retrofit2.http.Query

interface PopularMoviesService {
    @GET("movie/popular")
    suspend fun getPopularMovies(@Query(value = "api_key") key: String = Constants.API_KEY): MovieListRemote
}