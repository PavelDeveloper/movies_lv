package com.ponomarenko.movies.data.services

import com.ponomarenko.movies.common.Constants
import com.ponomarenko.movies.data.models.GenreListRemote
import retrofit2.http.GET
import retrofit2.http.Query

interface GenresService {
    @GET("genre/movie/list")
    suspend fun getGenres(@Query(value = "api_key") key: String= Constants.API_KEY): GenreListRemote
}