package com.ponomarenko.movies.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BelongsToCollectionRemote(
    @SerialName("backdrop_path") val backdropPath: String = "",
    val id: Int,
    val name: String = "",
    @SerialName("poster_path") val posterPath: String = ""
)