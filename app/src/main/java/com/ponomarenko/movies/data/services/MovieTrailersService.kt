package com.ponomarenko.movies.data.services

import com.ponomarenko.movies.common.Constants
import com.ponomarenko.movies.data.models.MovieTrailerListRemote
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieTrailersService {
    @GET("movie/{id}/videos")
    suspend fun getMovieTrailers(@Path("id") movieId: Int,
                         @Query("api_key") api_key: String = Constants.API_KEY): MovieTrailerListRemote

}