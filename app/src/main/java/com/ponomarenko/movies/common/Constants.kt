package com.ponomarenko.movies.common

internal object Constants {
    const val API_KEY  = "75cb25a125d25ee5271f3f5bdbb07415"
    const val BASE_URL  = "https://api.themoviedb.org/3/"
    const val YOUTUBE_API_KEY  = "AIzaSyBtgsM_fVTG0o6uORrFQbrMmmaJCt0SA3M"
    const val POSTER_BASE_URL  = "https://image.tmdb.org/t/p/w1280"
    const val YOUTUBE_IMAGE_URL  = "https://img.youtube.com/vi/%s/sddefault.jpg"
}